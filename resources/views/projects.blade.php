@extends('layout')

@section('content')
				<script type="text/javascript" src="{{{ VIGILANTMEDIA_CDN_BASE_URI }}}/web/js/facebox.js"></script>
				<script type="text/javascript">
				var facebox_options = {
					closeImage: '{{ VIGILANTMEDIA_CDN_BASE_URI }}/web/images/closelabel.gif',
					loadingImage: '{{ VIGILANTMEDIA_CDN_BASE_URI }}/web/images/loading.gif'
				};
				$(function () {
					$('a[rel*=facebox]').facebox(facebox_options);
				});
				</script>

				<header>
					<h1>Projects</h1>
				</header>
				
				<section id="current" class="full-column-last">
					<header>
						<h2>Current</h2>
					</header>
					
					<div class="row">
						<article class="two-column-single col-md-6">

							<h3><a href="https://bitbucket.org/observantrecords/profile/repositories">Observant Records Network</a></h3>

							<a href="https://bitbucket.org/observantrecords/profile/repositories"><img src="/assets/images/observant_records_o_square_logo.png" class="img-align-right" alt="[Observant Records]" title="[Observant Records]" width="128" /></a>

							<p>WordPress powers the official sites of my various music projects, including <a href="http://observantrecords.com/">Observant Records</a>, <a href="http://eponymous4.com/">Eponymous 4</a>, <a href="http://emptyensemble.com/">Empty Ensemble</a> and <a href="http://shinkyokuadvocacy.com/">Shinkyoku Advocacy</a>.</p>

							<p>An <a href="https://bitbucket.org/observantrecords/observant-records-administration">administration site</a> maintains release information, which is accessed through a <a href="https://bitbucket.org/observantrecords/observant-records-artist-connector-for-wordpress">custom-built WordPress plug-in</a>.</p>

							<p>The source code for the network is available to view on <a href="https://bitbucket.org/observantrecords/profile/repositories">Bitbucket</a> or <a href="https://github.com/observantrecords?tab=repositories">Github</a>.</p>

						</article>

						<article class="two-column-single-last col-md-6">

							<h3><a href="https://bitbucket.org/NemesisVex/profile/repositories">Vigilant Media Network</a></h3>

							<a href="https://bitbucket.org/NemesisVex/profile/repositories"><img src="/assets/images/wordpress-logo-notext-rgb.png" class="img-align-right" alt="[Wordpress]" title="[Wordpress]" width="128" /></a>

							<p>The Vigilant Media Network is a set of personal web projects run on WordPress or built with frameworks such as CodeIgniter and Laravel.</p>

							<p><a href="http://archive.musicwhore.org/">Musicwhore.org Archive</a> is a WordPress blog with a <a href="https://bitbucket.org/NemesisVex/musicwhore-artist-connector-for-wordpress">custom plugin</a> that connects to an <a href="https://bitbucket.org/NemesisVex/musicwhore.org-administration">external database</a> and the Amazon Affiliate Marketing API.</p>

							<p><a href="https://bitbucket.org/NemesisVex/movable-type-id-mapper-for-wordpress">Another plugin</a> maps Movable Type entry IDs with posts imported into Wordpress.</p>

							<p>The source code for the network is available to view on <a href="https://bitbucket.org/NemesisVex/profile/repositories">Bitbucket</a> or <a href="https://github.com/NemesisVex?tab=repositories">Github</a>.</p>

						</article>
					</div>

				</section>
@stop